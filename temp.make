; Drush Make (http://drupal.org/project/drush_make)
api = 2

; Drupal core

core = 6.x
projects[drupal][version] = 6.24
projects[drupal][patch][624] = http://drupalcode.org/project/mixed_session.git/blob/refs/heads/6.x-1.x://patches/mixed_session_core-6-24.patch

; Dependencies

projects[mixed_session][subdir] = contrib
projects[mixed_session][version] = 1.0
