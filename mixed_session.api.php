<?php
/**
 * @file
 * Documents hooks provided by this module.
 *
 * @author Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alters the mixed session redirect setting.
 *
 * This hook is called during the DRUPAL_BOOTSTRAP_LATE_PAGE_CACHE phase when
 * hook_boot() is invoked.
 *
 * @see drupal_bootstrap()
 */
function hook_mixed_session_boot() {
  mixed_session_security_mode_set(TRUE);
}

/**
 * Alters the mixed session redirect setting.
 *
 * This hook is called during the generation of themed output for the page when
 * MODULE_preprocess_page() is invoked.
 *
 * @see theme()
 * @see MODULE_preprocess_HOOK()
 */
function hook_mixed_session_page() {
  mixed_session_security_mode_set(FALSE);
  // Set this session variable to avoid a race condition on redirects.
  $_SESSION['mixed_session_form_redirect'] = TRUE;
}

/**
 * @} End of "addtogroup hooks".
 */
