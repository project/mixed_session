
CONTENTS OF THIS FILE
---------------------

 * Author
 * Description
 * Disclaimer
 * Installation
 * Dependencies
 * Inspiration
 * Developers

AUTHOR
------
Jim Berry ("solotandem", http://drupal.org/user/240748)

DESCRIPTION
-----------
This module enables mixed HTTP(S) sessions for a site. For example, a shopping
site may prefer to display product content in insecure mode (HTTP) while being
able to protect sensitive content (e.g credit card details and authenticated
user account information) using secure mode (HTTPS). The use of mixed sessions
adds complications such as preserving session data (e.g. a cart ID) across
session mode and protecting against session hijacking (with tools such as
Firesheep). This module addresses both concerns.

DISCLAIMER
----------
If your site becomes inaccessible after enabling the Mixed Session module
(e.g. because your server is not configured for HTTPS), you can disable
redirects by adding the following line to the end of your settings.php file:

    $conf['mixed_session_redirect_rule'] = 0;

Using Drush, from a command line, simply invoke:

    drush dis mixed_session

INSTALLATION
------------
To use this module, install it in a modules directory. See
http://drupal.org/node/895232 for further information.

Before installation of the Mixed Session module, two preparatory steps need
to be performed:
- add a secure session ID column and index to the sessions table.
- apply a patch against Drupal core.

The first step will be satisfied by installing the "Mixed Session Core" module
(located in the "modules" subdirectory of this module), a dependency of this
module. Using Drush, from a command line, simply invoke:

  drush en ms_core

To apply a patch against Drupal core, simply invoke:

  patch -p1 < /path/to/this/patch/mixed_session_core-6-NN.patch

or refer to http://drupal.org/patch/apply for use with git.

[The included Drush Make file provides a convenient method of completing the two
preparatory steps. The sample make file will download Drupal, this project, and
apply the core patch. From a command line, simply invoke:] [This may not apply
changes in the order needed to avoid PHP errors.]

  drush make --yes --contrib-destination=. temp.make

DEPENDENCIES
------------
While Mixed Session Core is the only module dependency for this module, the
implied dependency is that HTTPS is properly configured on your server before
installing this module. You must also apply the core patch.

INSPIRATION
-----------
This module enables mixed HTTP(S) sessions for a site by:
- back-porting the Drupal 7 dual session ID approach
- transmitting the secure ID only when in secure mode
- regenerating the insecure session ID when switching to secure mode
- preserving session data between session modes
- implementing paths on which to enter and exit secure mode
- providing a choice to stay secure once having entered secure mode

The design represents a back-port of the Drupal 7 dual session ID approach
with enhancements proposed and discussed in core issues for Drupal 7 and 8
to retain the session data when switching from insecure to secure mode.

While not impossible, one goal of this module is to make session hijacking
less likely or more difficult to accomplish.

The ideal approach suggested by some may be to enforce secure mode as soon as a
session is stepped-up to HTTPS. This module provides a setting for that purpose.

The code for this module was developed after reviewing:
- the session443, securepages (and prevent_hijack), and uc_ssl modules
- Drupal 7 and 8 dual session ID approach
- http://drupal.org/node/1131986
  - http://drupal.org/files/core-session_rebuild-1131986-19.patch
- http://drupal.org/node/1050746

DEVELOPERS
----------
If you want to create complex redirect rules, then implement the hooks provided
by this module. Refer to the api.php file included with this module.
