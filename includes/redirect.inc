<?php
/**
 * @file
 * Provides path and user redirect rule implementations.
 *
 * @author Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * Returns TRUE if http(s) mode should be switched.
 */
function mixed_session_mode_switch() {
  global $is_https;

  list($insecure_session_name, $session_name) = _session_names();
  $sid = $_COOKIE[$insecure_session_name];
  $ssid = isset($session_name) ? $_COOKIE[$session_name] : ''; // Cookie not sent if request is insecure.
  $user = session_user_load($sid, $insecure_session_name);

  $mixed_mode = variable_get('https', FALSE);
  $stay_secure = variable_get('mixed_session_stay_secure', MIXED_SESSION_STAY_SECURE) && isset($_SESSION['stay_secure']) && $_SESSION['stay_secure'];
  $enter_secure = _mixed_session_secure_enter();
  $exit_secure = _mixed_session_secure_exit();
  $secure_sid_exists = !empty($user->ssid);

  if (!$mixed_mode) {
    // @todo Does this make sense?
    return $is_https;
  }

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // This is a secondary request related to the main request.
    // This is an AJAX callback.
    // @todo Add this check to ignored pages.
    return $is_https;
  }

  // Rule precedence
  // - form (captured in hook_form_alter)
  // - page (or path) (captured here)
  // - user (captured here)

  // Path rules.
  // @todo The path "/user" follows the rules of this module. So add a condition for it.
  // Enter on "/user", then exit based on user state rules.

  // @todo Is there a good mechanism to determine when to exit secure mode?
  if (!$is_https) {
    // Request is insecure.
    if ($secure_sid_exists && $stay_secure && !$exit_secure) {
      // There is an https cookie and we are to remain secure.
      return TRUE;
    }
    elseif ($enter_secure) {
      // Page is to be secured.
      $_SESSION['stay_secure'] = TRUE;
      return TRUE;
    }
  }

  if ($is_https) {
    // Request is secure.
    if ($exit_secure) {
      unset($_SESSION['stay_secure']);
      $_SESSION['exit_secure'] = TRUE;
      return FALSE;
    }
    elseif ($enter_secure) {
      // Page is to be secured.
      $_SESSION['stay_secure'] = TRUE;
      return TRUE;
    }
    elseif ($stay_secure) {
      // Page is to be secured.
      return TRUE;
    }
    elseif (!$enter_secure && !$stay_secure) {
      // Page is not in list to be secured and we are not to remain secure.
      // At this point, we should defer to user rules.
//       return FALSE;
    }
  }

  // User state rules.
  if (mixed_session_user_redirect_none()) {
    return $is_https;
  }
  elseif (!$user->uid) {
    // Anonymous user: redirect.
    return FALSE;
  }
  if ($user->uid) {
    // Authenticated user: redirect.
    return variable_get('mixed_session_authenticated_rule', MIXED_SESSION_AUTHENTICATED_SECURE) == MIXED_SESSION_AUTHENTICATED_SECURE;
  }

  // Set "intended mode" so it matches request.
  return $is_https;
}

/**
 * Returns TRUE if user redirection is not enabled.
 *
 * @return boolean
 */
function mixed_session_user_redirect_none() {
  global $user;

  if (!$user->uid && variable_get('mixed_session_anonymous_rule', MIXED_SESSION_ANONYMOUS_INSECURE) == MIXED_SESSION_ANONYMOUS_NONE) {
    // Anonymous user: no redirect.
    return TRUE;
  }
  if ($user->uid && variable_get('mixed_session_authenticated_rule', MIXED_SESSION_AUTHENTICATED_SECURE) == MIXED_SESSION_AUTHENTICATED_NONE) {
    // Authenticated user: no redirect.
    return TRUE;
  }
  return FALSE;
}

/**
 * Returns TRUE if secure mode should be entered on this page.
 *
 * @return boolean
 */
function _mixed_session_secure_enter() {
  $pages = variable_get('mixed_session_enter_pages', MIXED_SESSION_ENTER_PAGES);
  return _mixed_session_path_match($_GET['q'], $pages);
}

/**
 * Returns TRUE if secure mode should be exited on this page.
 *
 * @return boolean
 */
function _mixed_session_secure_exit() {
  $pages = variable_get('mixed_session_exit_pages', MIXED_SESSION_EXIT_PAGES);
  return _mixed_session_path_match($_GET['q'], $pages);
}
