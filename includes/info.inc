<?php
/**
 * @file
 * Provides info-type hook implementations that are infrequently called.
 *
 * @author Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * Implements hook_help().
 */
function _mixed_session_help($section) {
  switch ($section) {
    case 'admin/help#mixed_session':
      // Return a line-break version of the module README.txt
      $readme = file_get_contents(drupal_get_path('module', 'mixed_session') . "/README.txt");
      if (function_exists('filter_filter')) {
        $readme = filter_filter('process', 1, 0, $readme);
        $readme = filter_filter('process', 1, 1, $readme);
      }
      return $readme;
    case 'admin/settings/mixed_session':
      return 'Pages and content are redirected to HTTP(S) based on the following rules applied in the order they appear below.';
      return 'Redirect pages and content to HTTP(S) based on rules applied in precedence order:' .
        '<ul>' .
        '<li>redirect rule</li>' .
        '<li>form rules</li>' .
        '<li>path rules</li>' .
        '<li>user state rules</li>' .
        '<li></li>' .
        '<li></li>' .
        '</ul>' .
        '' .
        '' .
        '';
  }
}

/**
 * Implements hook_menu().
 */
function _mixed_session_menu() {
  $items['admin/settings/mixed_session'] = array(
    'title' => 'Mixed Session',
    'description' => 'Configure mixed mode HTTP(S) session redirects.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mixed_session_settings_form'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'includes/admin.inc',
  );
  // Callback to test the HTTPS configuration of the server.
  $items['mixed_session_https_test'] = array(
    'title' => 'Mixed Session HTTPS test page',
    'description' => 'Sample page to request for testing server configuration.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mixed_session_https_test_form'),
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
    'file' => 'includes/admin.inc',
  );
  return $items;
}
