<?php
/**
 * @file
 * Provides administrative page callback implementations.
 *
 * @author Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * Form builder.
 * 
 * @ingroup forms
 * @see mixed_session_settings_form_submit()
 */
function mixed_session_settings_form(&$form_state) {
  // Redirect rule.
  $form['redirect_rules'] = array(
    '#type' => 'fieldset',
    '#title' => t('Redirect rule'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['redirect_rules']['mixed_session_redirect_rule'] = array(
    '#type' => 'radios',
    '#title' => t('Redirects'),
    '#default_value' => _mixed_session_https_test() ? variable_get('mixed_session_redirect_rule', MIXED_SESSION_REDIRECT_NONE) : MIXED_SESSION_REDIRECT_NONE,
    '#options' => array(
      MIXED_SESSION_REDIRECT_NONE => t('None - No redirection will occur.'),
      MIXED_SESSION_REDIRECT_MIXED => t('Mixed - Redirection will occur according to the rules below.'),
      MIXED_SESSION_REDIRECT_ALL => t('All - All pages will be redirected to HTTPS.'),
    ),
    // @todo This test is not conclusive and may cause problems.
    '#disabled' => !_mixed_session_https_test(),
    '#description' => t('A secure page request is run against this server. If these buttons are disabled, then this test has failed and it appears the web server has not been properly configured for HTTPS. If your site becomes inaccessible after enabling this module, please refer to the README.txt file.'),
  );

  // Content (or form) rules.
  $form['form_rules'] = array(
    '#type' => 'fieldset',
    '#title' => t('Form rules'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['form_rules']['mixed_session_form_rule'] = array(
    '#type' => 'radios',
    '#title' => t('Form rule'),
    '#default_value' => variable_get('mixed_session_form_rule', MIXED_SESSION_FORM_SECURE),
    '#options' => array(
      MIXED_SESSION_FORM_NONE => t('None: Do not redirect or modify any pages with the form.'),
      MIXED_SESSION_FORM_SUBMIT_SECURE => t('Mixed: Change the form action to submit to a secure URL.'),
      MIXED_SESSION_FORM_SECURE => t('Secure: Redirect all pages with the form to HTTPS.'),
    ),
    '#description' => t('If some of the indicated form IDs is displayed on many pages (e.g. the login block), then selecting "Secure" will require more server resources and may degrade the site performance. With a "Mixed" rule, the browser will not display a lock icon on these pages (<a href="http://drupal.org/node/812572#comment-3897304">more information</a>).'),
  );
  $form['form_rules']['mixed_session_form_ids'] = array(
    '#type' => 'textarea',
    '#title' => t('Form IDs'),
    '#description' => t('Enter the Drupal form IDs to apply this rule to, one per line.'),
    '#wysiwyg' => FALSE,
    '#default_value' => variable_get('mixed_session_form_ids', MIXED_SESSION_FORM_IDS),
  );

  // Path rules.
  // @todo This description is not being displayed; CSS or vertical tabs conflict?
  $form['mixed_session_path_rules'] = array(
    '#type' => 'fieldset',
    '#title' => t('Path rules'),
    '#description' => t('In each textarea below, enter one path per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.', array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>')),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['mixed_session_path_rules']['mixed_session_enter_pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Enter secure mode'),
    '#description' => t('Redirection to secure mode will be done for the pages listed here.'),
    '#wysiwyg' => FALSE,
    '#default_value' => variable_get('mixed_session_enter_pages', MIXED_SESSION_ENTER_PAGES),
  );
  $form['mixed_session_path_rules']['mixed_session_stay_secure'] = array(
    '#type' => 'checkbox',
    '#title' => t('Stay secure'),
    '#description' => t('If checked, then redirection to secure mode will continue on all requests after encountering a path to enter secure mode (above) and before encountering a path to exit secure mode (below).'),
    '#default_value' => variable_get('mixed_session_stay_secure', MIXED_SESSION_STAY_SECURE),
  );
  $form['mixed_session_path_rules']['mixed_session_exit_pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Exit secure mode'),
    '#description' => t('Redirection to insecure mode will be done for the pages listed here.'),
    '#wysiwyg' => FALSE,
    '#default_value' => variable_get('mixed_session_exit_pages', MIXED_SESSION_EXIT_PAGES),
  );
  $form['mixed_session_path_rules']['mixed_session_ignore_pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Ignored paths'),
    '#description' => t('The paths listed here will be ignored and no redirection will be done. Requests with a 404 status are always ignored.'),
    '#wysiwyg' => FALSE,
    '#default_value' => variable_get('mixed_session_ignore_pages', MIXED_SESSION_IGNORE_PAGES),
  );

  // User state rules.
  $form['user_rules'] = array(
    '#type' => 'fieldset',
    '#title' => t('User state rules'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['user_rules']['mixed_session_anonymous_rule'] = array(
    '#type' => 'radios',
    '#title' => t('Anonymous user'),
    '#default_value' => variable_get('mixed_session_anonymous_rule', MIXED_SESSION_ANONYMOUS_INSECURE),
    '#options' => array(
      MIXED_SESSION_AUTHENTICATED_NONE => t('None - No redirection will occur.'),
      MIXED_SESSION_AUTHENTICATED_INSECURE => t('Insecure - Redirect anonymous users to HTTP.'),
    ),
    '#description' => t('Redirecting anonymous users to insecure mode (HTTP) may improve performance as HTTP pages have much faster response times.'),
  );
  $form['user_rules']['mixed_session_authenticated_rule'] = array(
    '#type' => 'radios',
    '#title' => t('Authenticated user'),
    '#default_value' => variable_get('mixed_session_authenticated_rule', MIXED_SESSION_AUTHENTICATED_SECURE),
    '#options' => array(
      MIXED_SESSION_AUTHENTICATED_NONE => t('None: No redirection will occur.'),
      MIXED_SESSION_AUTHENTICATED_INSECURE => t('Insecure: Redirect authenticated users to HTTP.'),
      MIXED_SESSION_AUTHENTICATED_SECURE => t('Secure: Redirect authenticated users to HTTPS.'),
    ),
    '#description' => t('Session hijacking is a concern for authenticated users when using mixed sessions. To prevent unauthorized access to sensitive content, authenticated users should be redirected to HTTPS on any page displaying content that should be protected from eavesdroppers.'),
  );

  $form['#submit'][] = 'mixed_session_settings_form_submit';

  return system_settings_form($form);
}

/**
 * Submit handler.
 *
 * @todo Not sure any of the messages still apply as:
 * - we should be guarding against hijacking
 * - secure sessions can be created without disruption of logged in users
 *
 * @see mixed_session_settings_form()
 */
function mixed_session_settings_form_submit($form, &$form_state) {
  $redirect = $form_state['values']['mixed_session_redirect_rule'];
  $form2 = $form_state['values']['mixed_session_form_rule'];
  $anonymous = $form_state['values']['mixed_session_anonymous_rule'];
  $authenticated = $form_state['values']['mixed_session_authenticated_rule'];

  $anonymous_insecure = ($redirect == MIXED_SESSION_REDIRECT_NONE || $anonymous == MIXED_SESSION_ANONYMOUS_NONE);
  $authenticated_insecure = ($redirect == MIXED_SESSION_REDIRECT_NONE || $authenticated == MIXED_SESSION_AUTHENTICATED_NONE);

  $previous_redirect = $form['redirect_rules']['mixed_session_redirect_rule']['#default_value'];
  $previous_form2 = $form['form_rules']['mixed_session_form_rule']['#default_value'];
  $previous_anonymous = $form['user_rules']['mixed_session_anonymous_rule']['#default_value'];
  $previous_authenticated = $form['user_rules']['mixed_session_authenticated_rule']['#default_value'];

  $previous_anonymous_insecure = ($previous_redirect == MIXED_SESSION_REDIRECT_NONE || $previous_anonymous == MIXED_SESSION_ANONYMOUS_NONE);
  $previous_authenticated_insecure = ($previous_redirect == MIXED_SESSION_REDIRECT_NONE || $previous_authenticated == MIXED_SESSION_AUTHENTICATED_NONE);

  if ($previous_redirect != $redirect ||
      $previous_form2 != $form2 ||
      $previous_authenticated_insecure != $authenticated_insecure ||
      $previous_anonymous_insecure != $anonymous_insecure) {
    drupal_set_message(t('Because you have changed the redirect rules, you may need to change the <a href="!status">settings.php file</a>.', array('!status' => url('admin/reports/status'))), 'warning');
  }
}

/**
 * Form builder.
 * 
 * @ingroup forms
 */
function mixed_session_https_test_form(&$form_state) {
  return array();
}

/**
 * Attempts to determine whether the site is accessible over HTTPS.
 *
 * @param boolean $reset
 *   Whether to reset a static variable.
 *
 * @return boolean
 */
function _mixed_session_https_test($reset = FALSE) {
  global $is_https;
  static $test;

  if (isset($test) && !$reset) {
    return $test;
  }

  if ($is_https) {
    // If the current request is in secure mode, then assume HTTPS is configured correctly.
    return $test = TRUE;
  }

  // Otherwise attempt to fetch the HTTPS test page in secure mode.
  // @todo If .htaccess authorization is in place, then this will fail.
  // Add text fields to input credentials on settings form?
  $url = _mixed_session_url_scheme_modify(url('mixed_session_https_test', array('absolute' => TRUE)));
  if (module_exists('shield')) {
    // With shield installed, this request will fail without credentials.
    $username = variable_get('shield_user', '');
    $password = variable_get('shield_pass', '');
    if ($username && $password) {
      $url = str_replace('https://', 'https://' . $username . ':' . $password . '@', $url);
    }
  }
  $response = drupal_http_request($url);
  return $test = ($response->code == 200);
}
