<?php
/**
 * @file
 * Provides form redirect rule implementations that are infrequently called.
 *
 * @author Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * Implements hook_form_alter().
 */
function _mixed_session_form_alter(&$form, &$form_state, $form_id) {
  switch (variable_get('mixed_session_form_rule', MIXED_SESSION_FORM_SECURE)) {
    case MIXED_SESSION_FORM_NONE:
      // This should not occur unless another module calls this routine directly.
      break;

    case MIXED_SESSION_FORM_SECURE:
      mixed_session_security_mode_set(TRUE);
      $_SESSION['mixed_session_form_redirect'] = TRUE;
      break;

    case MIXED_SESSION_FORM_SUBMIT_SECURE:
      $action = parse_url($form['#action']);
      if (!$action) {
        // Failed to parse the URL.
        return;
      }

      // Change the form action to submit to a secure path.
      if (!empty($action['scheme'])) {
        if ($action['scheme'] == 'http') {
          // An absolute URL.
          $form['#action'] = _mixed_session_url_scheme_modify($form['#action']);
        }
      }
      elseif (!empty($action['host'])) {
        // A scheme-relative URL.
        $form['#action'] = _mixed_session_url_scheme_modify('http://' . $form['#action']);
      }
      else {
        // A relative or internal URL.

        // Remove the base path.
        $action['path'] = preg_replace('#^' . preg_quote(base_path()) . '#', '', $action['path']);

        // Ensure the parsed URL has all parts.
        $action_default = array(
          'path' => '',
          'query' => '',
          'fragment' => '',
        );
        $action = array_merge($action_default, $action);

        // Build an absolute URL.
        $form['#action'] = url($action['path'], array('absolute' => TRUE, 'query' => $action['query'], 'fragment' => $action['fragment']));
        $form['#action'] = _mixed_session_url_scheme_modify($form['#action']);
      }
      break;
  }
}
